package task3;

import common.ArraysService;

import java.util.Arrays;

class CustomerList {
    private final ArraysService arraysService = new ArraysService();
    private Customer[] customers;

    public CustomerList(Customer[] customers) {
        this.customers = customers;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    public Customer[] findCustomersInAlphabeticalOrder() {
        Customer[] result = Arrays.copyOf(this.customers, this.customers.length);
        sortCustomersByNameAndSurname(result);
        return result;
    }

    public Customer[] findCustomersWithCreditCardNumberInRangeInclusive(String start, String end) {
        Customer[] result = new Customer[this.customers.length];
        int resultSize = 0;

        for (Customer customer : this.customers) {
            boolean largerOrEqual = customer.getCreditCardNumber().trim().compareTo(start.trim()) >= 0;
            boolean lessOreEqual = customer.getCreditCardNumber().trim().compareTo(end.trim()) <= 0;
            if (largerOrEqual && lessOreEqual) {
                result[resultSize] = customer;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);
    }

    private void sortCustomersByNameAndSurname(Customer[] customers) {
        for (int i = 0; i < customers.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < customers.length; j++) {
                int compareNames = customers[j].getFirstName().compareTo(customers[minIndex].getFirstName());
                int compareSurnames = customers[j].getSurname().compareTo(customers[minIndex].getSurname());
                if (compareNames < 0 || (compareNames == 0 && compareSurnames < 0)) {
                    minIndex = j;
                }
            }
            arraysService.swapElements(customers, i, minIndex);
        }
    }
}
