package task3;

import common.ArraysService;
import datagenerators.CustomerGenerator;

/**
 * Create a Customer class, the specification of which is given below. Define constructors, set and get methods,
 * and the toString() method. Create a second class aggregating an array of type Customer, with suitable constructors and methods.
 * Set data selection criteria and output this data to the console.
 * <p>
 * Customer class: id, surname, first name, patronymic, address, credit card number, bank account number.
 * Find and output:
 * a) list of buyers in alphabetical order;
 * b) a list of customers whose credit card number is in the specified interval
 */
public class Main {

    public static void main(String[] args) {
        CustomerGenerator customerGenerator = new CustomerGenerator();
        Customer[] customers = customerGenerator.generateCustomers();
        CustomerList customerList = new CustomerList(customers);

        ArraysService arraysService = new ArraysService();
        arraysService.printArray("Initial customers:", customerList.getCustomers());

        Customer[] customersInAlphabeticalOrder = customerList.findCustomersInAlphabeticalOrder();
        arraysService.printArray("Customers in alphabetical order (by name and surname):",
                customersInAlphabeticalOrder);

        String start = "0000 0000 0000 0000";
        String end = "5555 5555 5555 5555";
        Customer[] customersWithCreditCardNumberInRange = customerList.findCustomersWithCreditCardNumberInRangeInclusive(start, end);
        arraysService.printArray("Customers with credit card number in range between " + start + " and " + end + ":",
                customersWithCreditCardNumberInRange);
    }
}
