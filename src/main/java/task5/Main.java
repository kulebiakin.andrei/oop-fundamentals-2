package task5;


import common.ArraysService;
import datagenerators.AirlineGenerator;

/**
 * Create the Airline class, the specification of which is given below. Define constructors, set and get methods,
 * and the toString() method. Create a second class aggregating an array of type Airline, with suitable constructors
 * and methods. Set data selection criteria and output this data to the console.
 * <p>
 * Airline: destination, flight number, aircraft type, departure time, days of the week.
 * <p>
 * Find and output:
 * a) list of flights for a given destination;
 * b) list of flights for a given day of the week;
 * c) a list of flights for a given day of the week, the departure time for which is longer than the specified one.
 */
public class Main {

    public static void main(String[] args) {
        AirlineGenerator airlineGenerator = new AirlineGenerator();
        Airline[] airlines = airlineGenerator.getAirlines();
        AirlineList airlineList = new AirlineList(airlines);

        ArraysService arraysService = new ArraysService();
        arraysService.printArray("Initial airlines:", airlineList.getAirlines());

        String destination = "New York";
        Airline[] airlinesByDestination = airlineList.findAirlinesByDestination(destination);
        arraysService.printArray("List of flights for a given destination - " + destination + ":",
                airlinesByDestination);

        String day = "Monday";
        Airline[] airlinesByDay = airlineList.findAirlinesByDay(day);
        arraysService.printArray("List of flights for a given day of week - " + day + ":", airlinesByDay);

        String departureTime = "10:00";
        Airline[] airlinesByDayAndDepartureTimeAfter = airlineList.findAirlinesByDayAndDepartureTimeAfter(day, departureTime);
        arraysService.printArray("List of flights for a given day - " + day + " - and departure time after - " + departureTime + ":",
                airlinesByDayAndDepartureTimeAfter);
    }
}
