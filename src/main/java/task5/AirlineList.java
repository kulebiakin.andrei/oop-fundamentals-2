package task5;

import common.ArraysService;

import java.util.Arrays;

public class AirlineList {
    private final ArraysService arraysService = new ArraysService();

    private Airline[] airlines;

    public AirlineList(Airline[] airlines) {
        this.airlines = airlines;
    }

    public Airline[] getAirlines() {
        return airlines;
    }

    public void setAirlines(Airline[] airlines) {
        this.airlines = airlines;
    }

    public Airline[] findAirlinesByDestination(String destination) {
        Airline[] result = new Airline[airlines.length];
        int resultSize = 0;

        for (Airline airline : airlines) {
            if (destination.equals(airline.getDestination())) {
                result[resultSize] = airline;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);
    }

    public Airline[] findAirlinesByDay(String day) {
        Airline[] result = new Airline[airlines.length];
        int resultSize = 0;

        for (Airline airline : airlines) {
            if (arraysService.arrayContains(airline.getDaysOfWeek(), day)) {
                result[resultSize] = airline;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);

    }

    public Airline[] findAirlinesByDayAndDepartureTimeAfter(String day, String departureTime) {
        Airline[] airlinesByDay = findAirlinesByDay(day);
        Airline[] result = new Airline[airlinesByDay.length];
        int resultSize = 0;

        for (Airline airline : airlinesByDay) {
            if (airline.getDepartureTime().compareTo(departureTime) > 0) {
                result[resultSize] = airline;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);
    }
}
