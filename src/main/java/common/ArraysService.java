package common;

public class ArraysService {
    private final NumberGenerationService numberGenerationService = new NumberGenerationService();

    public  <T> void printArray(String description, T[] array) {
        System.out.println(description);
        printArray(array);
        System.out.println();
    }

    public  <T> void printArray(T[] array) {
        for (T element : array) {
            System.out.println(element);
        }
    }

    public <T> void swapElements(T[] array, int index1, int index2) {
        T temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    public <T> boolean arrayContains(T[] array, T element) {
        for (T s : array) {
            if (element.equals(s)) {
                return true;
            }
        }
        return false;
    }

    public <T> T getRandomElement(T[] array) {
        int random = numberGenerationService.getRandomInt(array.length);
        return array[random];
    }
}
