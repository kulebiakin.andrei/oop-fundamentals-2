package common;

public class TextGenerationService {
    private final ArraysService arraysService = new ArraysService();

    public String getRandomName() {
        return arraysService.getRandomElement(getNames());
    }

    public String getRandomSurname() {
        return arraysService.getRandomElement(getSurnames());
    }

    public String getRandomBookBindingType() {
        return arraysService.getRandomElement(getBookBindings());
    }

    public String getRandomBookPublisher() {
        return arraysService.getRandomElement(getPublishers());
    }

    public String getRandomBookTitle() {
        String adjective = arraysService.getRandomElement(getAdjectives());
        String noun = arraysService.getRandomElement(getNouns());
        String famousPerson = arraysService.getRandomElement(getFamousPersons());
        return String.format("%s %s of %s", adjective, noun, famousPerson);
    }

    private String[] getNames() {
        return new String[]{
                "Aled",
                "Tyrese",
                "Imogen",
                "Abdul",
                "Katy",
                "Christina",
                "Jonathan",
                "Anna",
                "Goran",
                "Lara",
        };
    }

    private String[] getSurnames() {
        return new String[]{
                "Dominguez",
                "Mora",
                "Donnelly",
                "Cervantes",
                "Moreno",
                "England",
                "Haas",
                "Ramsey",
                "Marsh",
                "Sampson",
        };
    }

    private String[] getBookBindings() {
        return new String[]{
                "Saddle stitch binding",
                "PUR binding",
                "Hardcover or case binding",
                "Singer sewn binding",
                "Section sewn binding",
                "Coptic stitch binding",
                "Wiro, comb or spiral binding",
                "Interscrew binding",
                "Japanese binding",
                "Solander boxes and slipcases"
        };
    }

    private String[] getPublishers() {
        return new String[]{
                "Crown Publishing",
                "Tin House Books",
                "Comma Press",
                "Melville House",
        };
    }

    private String[] getAdjectives() {
        return new String[]{
                "Dark",
                "Mysterious",
                "Famous",
                "Forgotten",
                "Endless"
        };
    }

    private String[] getNouns() {
        return new String[]{
                "Secrets",
                "Myths",
                "Lies",
                "Whispers",
                "Legends"
        };
    }

    private String[] getFamousPersons() {
        return new String[]{
                "Dorian Grey",
                "Harry Potter",
                "Eminem",
                "Queen Elizabeth I",
                "Albert Einstein",
        };
    }
}
