package common;

import java.util.Random;

public class NumberGenerationService {

    public int getRandomInt(int min, int max) {
        return getRandomInt(max - min) + min;
    }

    public int getRandomInt(int bound) {
        return new Random().nextInt(bound);
    }

    public int[] generateRandomIntArray(int size) {
        int[] result = new int[size];

        for (int i = 0; i < result.length; i++) {
            result[i] = getRandomInt(10);
        }

        return result;
    }

    public String generateRandomBankAccountNumber(int numberLength) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < numberLength; i++) {
            stringBuilder.append(getRandomInt(9));
        }

        return stringBuilder.toString();
    }

    public String generateRandomCreditCardNumber() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                stringBuilder.append(getRandomInt(9));
            }
            stringBuilder.append(" ");
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }
}
