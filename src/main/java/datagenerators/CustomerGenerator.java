package datagenerators;

import common.NumberGenerationService;
import common.TextGenerationService;
import task3.Customer;

public class CustomerGenerator {
    private final TextGenerationService textGenerationService = new TextGenerationService();
    private final NumberGenerationService numberGenerationService = new NumberGenerationService();

    public Customer[] generateCustomers() {
        Customer[] customers = new Customer[10];

        for (int i = 0; i < customers.length; i++) {
            Customer customer = new Customer();
            customer.setId(i);
            customer.setFirstName(textGenerationService.getRandomName());
            customer.setSurname(textGenerationService.getRandomSurname());
            customer.setPatronymic(textGenerationService.getRandomName());
            customer.setAddress("Ulitsa Stroiteley b. " + i);
            customer.setBankAccountNumber(numberGenerationService.generateRandomBankAccountNumber(10));
            customer.setCreditCardNumber(numberGenerationService.generateRandomCreditCardNumber());
            customers[i] = customer;
        }

        return customers;
    }
}
