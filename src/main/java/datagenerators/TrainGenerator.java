package datagenerators;

import task2.Train;

public class TrainGenerator {

    public Train[] getTrains() {
        Train[] trains = new Train[5];
        trains[0] = new Train("Beograd", 56, "09:00");
        trains[1] = new Train("Zajecar", 123, "12:00");
        trains[2] = new Train("Novi Sad", 89, "13:00");
        trains[3] = new Train("Sarajevo", 7189, "23:00");
        trains[4] = new Train("Zajecar", 123, "09:30");
        return trains;
    }
}
