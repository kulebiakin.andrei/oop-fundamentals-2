package datagenerators;

import common.NumberGenerationService;
import common.TextGenerationService;
import task4.Book;

public class BookGenerator {
    private final NumberGenerationService numberGenerationService = new NumberGenerationService();
    private final TextGenerationService textGenerationService = new TextGenerationService();

    public Book[] generateBooks(int size) {
        Book[] books = new Book[size];

        for (int i = 0; i < books.length; i++) {
            Book book = new Book();
            book.setId(i);
            book.setTitle(textGenerationService.getRandomBookTitle());
            book.setAuthors(generateAuthors(numberGenerationService.getRandomInt(1, 4)));
            book.setPublisher(textGenerationService.getRandomBookPublisher());
            book.setPublicationYear(numberGenerationService.getRandomInt(1900, 2023));
            book.setNumberOfPages(numberGenerationService.getRandomInt(10, 2_000));
            book.setPrice(numberGenerationService.getRandomInt(100, 10_000));
            book.setBindingType(textGenerationService.getRandomBookBindingType());
            books[i] = book;
        }

        return books;
    }

    public String[] generateAuthors(int size) {
        String[] authors = new String[size];

        for (int i = 0; i < authors.length; i++) {
            authors[i] = textGenerationService.getRandomName() + " " + textGenerationService.getRandomSurname();
        }

        return authors;
    }
}
