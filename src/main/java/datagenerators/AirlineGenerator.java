package datagenerators;

import task5.Airline;

public class AirlineGenerator {

    public Airline[] getAirlines() {
        Airline[] airlines = new Airline[5];
        airlines[0] = new Airline("Belgrade", 56, "Passenger plane", "09:00", new String[]{"Sunday", "Monday"});
        airlines[1] = new Airline("New York", 123, "Cargo plane", "12:00", new String[]{"Monday", "Tuesday", "Saturday"});
        airlines[2] = new Airline("Berlin", 89, "Passenger plane", "13:00", new String[]{"Wednesday", "Friday", "Saturday"});
        airlines[3] = new Airline("Istanbul", 7189, "Military plane", "23:00", new String[]{"Sunday", "Friday"});
        airlines[4] = new Airline("New York", 123, "Passenger plane", "09:30", new String[]{"Monday", "Wednesday", "Friday"});
        return airlines;
    }
}
