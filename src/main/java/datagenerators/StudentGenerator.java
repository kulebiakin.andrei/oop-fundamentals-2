package datagenerators;

import common.NumberGenerationService;
import common.TextGenerationService;
import task1.Student;

public class StudentGenerator {
    private final TextGenerationService textGenerationService = new TextGenerationService();
    private final NumberGenerationService numberGenerationService = new NumberGenerationService();

    public Student[] generateStudents(int size) {
        Student[] students = new Student[size];

        for (int i = 0; i < students.length; i++) {
            Student student = new Student();
            student.setName(textGenerationService.getRandomName());
            student.setSurname(textGenerationService.getRandomSurname());
            student.setGroupNumber(numberGenerationService.getRandomInt(5));
            student.setGrades(numberGenerationService.generateRandomIntArray(5));
            students[i] = student;
        }

        return students;
    }
}
