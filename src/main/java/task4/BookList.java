package task4;

import common.ArraysService;

import java.util.Arrays;

public class BookList {
    private final ArraysService arraysService = new ArraysService();
    private Book[] books;

    public BookList(Book[] books) {
        this.books = books;
    }

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public Book[] findBooksByAuthor(String author) {
        Book[] result = new Book[books.length];
        int resultSize = 0;

        for (Book book : books) {
            String[] authors = book.getAuthors();
            if (arraysService.arrayContains(authors, author)) {
                result[resultSize] = book;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);
    }

    public Book[] findBooksByPublisher(String publisher) {
        Book[] result = new Book[books.length];
        int resultSize = 0;

        for (Book book : books) {
            if (publisher.equals(book.getPublisher())) {
                result[resultSize] = book;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);
    }

    public Book[] findBooksReleasedAfterYear(int year) {
        Book[] result = new Book[books.length];
        int resultSize = 0;

        for (Book book : books) {
            if (book.getPublicationYear() > year) {
                result[resultSize] = book;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);
    }
}
