package task4;

import common.ArraysService;
import common.TextGenerationService;
import datagenerators.BookGenerator;

/**
 * Create a Book class, the specification of which is given below. Define constructors, set and get methods,
 * and the toString() method. Create a second class aggregating an array of type Book, with suitable constructors and methods.
 * Set data selection criteria and output this data to the console.
 * <p>
 * Book: id, title, author(s), publisher, year of publication, number of pages, price, type of binding.
 * <p>
 * Find and output:
 * a) a list of books by a given author;
 * b) a list of books published by a given publisher;
 * c) a list of books released after a given year.
 */
public class Main {

    public static void main(String[] args) {
        BookGenerator bookGenerator = new BookGenerator();
        Book[] books = bookGenerator.generateBooks(10);
        BookList bookList = new BookList(books);

        ArraysService arraysService = new ArraysService();
        arraysService.printArray("Initial books:", bookList.getBooks());

        Book book = arraysService.getRandomElement(bookList.getBooks());
        String author = arraysService.getRandomElement(book.getAuthors());
        Book[] booksByAuthor = bookList.findBooksByAuthor(author);
        arraysService.printArray("Books by a given author " + author + ":", booksByAuthor);

        TextGenerationService textGenerationService = new TextGenerationService();
        String publisher = textGenerationService.getRandomBookPublisher();
        Book[] booksByPublisher = bookList.findBooksByPublisher(publisher);
        arraysService.printArray("Books published by a given publisher " + publisher + ":", booksByPublisher);

        int year = 1940;
        Book[] booksReleasedAfterYear = bookList.findBooksReleasedAfterYear(year);
        arraysService.printArray("Books released after a given year " + year + ":", booksReleasedAfterYear);
    }
}
