package task2;

import common.ArraysService;
import datagenerators.TrainGenerator;

/**
 * Create a Train class containing the fields: destination name, train number, departure time.
 * Create data in an array of five Train type elements, add the ability to sort the array elements by train numbers.
 * Add the ability to display information about the train whose number is entered by the user.
 * Add the ability to sort the array by destination, and trains with the same destinations should be ordered by departure time.
 */
public class Main {

    public static void main(String[] args) {
        TrainGenerator trainGenerator = new TrainGenerator();
        Train[] trains = trainGenerator.getTrains();
        TrainList trainList = new TrainList(trains);

        ArraysService arraysService = new ArraysService();
        arraysService.printArray("Initial trains:", trains);

        Train[] trainsSortedByTrainNumber = trainList.getTrainsSortedByTrainNumber();
        arraysService.printArray("Trains sorted by Train Number:", trainsSortedByTrainNumber);

        int trainNumber = 123;
        Train[] trainsByTrainNumber = trainList.findTrainsByTrainNumber(trainNumber);
        arraysService.printArray("Find trains by Train Number " + trainNumber + ":", trainsByTrainNumber);

        Train[] trainsSortedByDestination = trainList.getTrainsSortedByDestination();
        System.out.println("Trains sorted by Destination and Departure Time:");
        arraysService.printArray(trainsSortedByDestination);
    }
}
