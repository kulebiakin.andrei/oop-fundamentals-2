package task2;

import common.ArraysService;

import java.util.Arrays;

public class TrainList {
    private final ArraysService arraysService = new ArraysService();
    private Train[] trains;

    public TrainList(Train[] trains) {
        this.trains = trains;
    }


    public Train[] getTrainsSortedByTrainNumber() {
        Train[] trainsCopy = Arrays.copyOf(trains, trains.length);

        for (int i = 0; i < trainsCopy.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < trainsCopy.length; j++) {
                if (trainsCopy[j].getTrainNumber() < trainsCopy[minIndex].getTrainNumber()) {
                    minIndex = j;
                }
            }
            arraysService.swapElements(trainsCopy, i, minIndex);
        }

        return trainsCopy;
    }

    public Train[] findTrainsByTrainNumber(int trainNUmber) {
        Train[] result = new Train[trains.length];
        int resultSize = 0;

        for (Train train : trains) {
            if (train.getTrainNumber() == trainNUmber) {
                result[resultSize] = train;
                resultSize++;
            }
        }

        return Arrays.copyOf(result, resultSize);
    }

    public Train[] getTrainsSortedByDestination() {
        Train[] trainsCopy = Arrays.copyOf(trains, trains.length);

        for (int i = 0; i < trainsCopy.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < trainsCopy.length; j++) {
                int compareDestinations = trainsCopy[j].getDestination().compareTo(trainsCopy[minIndex].getDestination());
                int compareDepartureTime = trainsCopy[j].getDepartureTime().compareTo(trainsCopy[minIndex].getDepartureTime());
                if (compareDestinations < 0 || (compareDestinations == 0 && compareDepartureTime < 0)) {
                    minIndex = j;
                }
            }
            arraysService.swapElements(trainsCopy, i, minIndex);
        }

        return trainsCopy;
    }

    public Train[] getTrains() {
        return trains;
    }

    public void setTrains(Train[] trains) {
        this.trains = trains;
    }
}
