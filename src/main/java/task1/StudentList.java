package task1;

import java.util.Arrays;

public class StudentList {
    private Student[] students;

    public StudentList(Student[] students) {
        this.students = students;
    }

    public Student[] findStudentsWithAllGradesGreaterThan(int targetGrade) {
        Student[] result = new Student[students.length];
        int resultNumber = 0;

        for (Student student : students) {
            int[] studentGrades = student.getGrades();
            if (isAllGradesGreaterOrEqualThan(studentGrades, targetGrade)) {
                result[resultNumber] = student;
                resultNumber++;
            }
        }

        return Arrays.copyOf(result, resultNumber);
    }

    public boolean isAllGradesGreaterOrEqualThan(int[] studentGrades, int targetGrade) {
        for (int grade : studentGrades) {
            if (grade < targetGrade) {
                return false;
            }
        }
        return true;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }
}
