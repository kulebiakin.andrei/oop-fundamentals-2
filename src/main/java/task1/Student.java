package task1;

import java.util.Arrays;
import java.util.Objects;

public class Student {
    private String name;
    private String surname;
    private int groupNumber;
    private int[] grades;

    public Student() {
    }

    public Student(String name, String surname, int groupNumber, int[] grades) {
        this.name = name;
        this.surname = surname;
        this.groupNumber = groupNumber;
        this.grades = grades;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Student student = (Student) o;

        if (groupNumber != student.groupNumber) {
            return false;
        }
        if (!Objects.equals(name, student.name)) {
            return false;
        }
        if (!Objects.equals(surname, student.surname)) {
            return false;
        }
        return Arrays.equals(grades, student.grades);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + groupNumber;
        result = 31 * result + Arrays.hashCode(grades);
        return result;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", groupNumber=" + groupNumber +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}
