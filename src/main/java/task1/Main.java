package task1;

import common.ArraysService;
import datagenerators.StudentGenerator;

/**
 * Create a class named Student, containing the fields: surname and initials, group number,
 * academic performance (an array of five elements).
 * Create an array of ten elements of this type. Add the ability to output the names and numbers of groups
 * of students with grades equal to only 9 or 10.
 */
public class Main {

    public static void main(String[] args) {
        StudentGenerator studentGenerator = new StudentGenerator();
        Student[] students = studentGenerator.generateStudents(10);
        StudentList studentList = new StudentList(students);

        ArraysService arraysService = new ArraysService();
        arraysService.printArray("All students:", students);

        int targetGrade = 3;
        Student[] studentsWithAllGradesGreaterThan = studentList.findStudentsWithAllGradesGreaterThan(targetGrade);

        arraysService.printArray("Students with grades greater than " + targetGrade + ":",
                studentsWithAllGradesGreaterThan);
    }
}
